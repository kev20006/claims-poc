import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaxComponentComponent } from './pax-component.component';

describe('PaxComponentComponent', () => {
  let component: PaxComponentComponent;
  let fixture: ComponentFixture<PaxComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaxComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaxComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
