import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ClaimService } from 'src/app/services/claim.service';

@Component({
  selector: 'app-pax-component',
  templateUrl: './pax-component.component.html',
  styleUrls: ['./pax-component.component.scss']
})
export class PaxComponent implements OnInit, OnDestroy {

  public paxDetails: any = {
    name: '',
    dob: '',
    address: '',
    postcode: '',
    city: '',
    country: ''
  };
  private formSub: Subscription;
  constructor(private claims: ClaimService) { }

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this.formSub = this.claims.formDetails.subscribe({
      next: (data) => this.paxDetails = data.paxComponent
    });
  }

  ngOnDestroy(): void {
    // clean up
    this.formSub.unsubscribe();
  }

}
