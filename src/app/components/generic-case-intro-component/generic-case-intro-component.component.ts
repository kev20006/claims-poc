import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-generic-case-intro-component',
  templateUrl: './generic-case-intro-component.component.html',
  styleUrls: ['./generic-case-intro-component.component.scss']
})
export class GenericCaseIntroComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
