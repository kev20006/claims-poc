import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericCaseIntroComponentComponent } from './generic-case-intro-component.component';

describe('GenericCaseIntroComponentComponent', () => {
  let component: GenericCaseIntroComponentComponent;
  let fixture: ComponentFixture<GenericCaseIntroComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenericCaseIntroComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericCaseIntroComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
