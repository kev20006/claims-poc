import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FlightDetials } from 'src/app/interfaces/FlightDetails';
import { ClaimService } from 'src/app/services/claim.service';

@Component({
  selector: 'app-flight-component',
  templateUrl: './flight-component.component.html',
  styleUrls: ['./flight-component.component.scss']
})
export class FlightComponent implements OnInit, OnDestroy {

  private claimsSub: Subscription;
  public flightDetails: FlightDetials;

  constructor(private claims: ClaimService) {
  }

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this.claimsSub = this.claims.formDetails.subscribe({
      next: data => this.flightDetails = data.flightComponent,
      error: err => console.log(err)
    });
  }

  ngOnDestroy(): void {
    this.claimsSub.unsubscribe();
  }

}
