import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BankDetails } from 'src/app/interfaces/BankDetails';
import { PaxDetails } from 'src/app/interfaces/PaxDetails';
import { ClaimService } from 'src/app/services/claim.service';

@Component({
  selector: 'app-bank-details-component',
  templateUrl: './bank-details-component.component.html',
  styleUrls: ['./bank-details-component.component.scss']
})
export class BankDetailsComponent implements OnInit, AfterViewChecked {

  public claimSub: Subscription;
  public paxDetails: PaxDetails | null = null;
  private prePopped = false;

  public bankDetails: BankDetails;
  constructor(private claims: ClaimService, private ref: ChangeDetectorRef) {}

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this.claimSub = this.claims.formDetails.subscribe({
      next: data => {
        if (data.paxComponent) {
          this.paxDetails = data.paxComponent;
        }
        this.bankDetails = data.BankDetailsComponent;
        console.log(this.bankDetails);
      }
    });
  }

  ngAfterViewChecked(): void {
    if (!this.prePopped) {
      setTimeout(() => {
        this.claims.addToFrom('BankDetailsComponent', this.prepop(this.bankDetails));
      }, 0);
      this.prePopped = true;
    }
  }

  private prepop(bankDetails): any {
    const prepopFields = {
      hasBeenPrepoped: true,
      name: '',
      differentName: false,
      differentBillingAddress: false,
      billingAddress: {
        address: '',
        postcode: '',
        city: '',
        country: ''
      }
    };

    if (this.paxDetails) {
      if (!bankDetails.differentName) {
        prepopFields.name = this.paxDetails.name;
      }
      if (!bankDetails.differentBillingAddress){
        const {address, postcode, city, country } = this.paxDetails;
        prepopFields.billingAddress =  {address, postcode, city, country};
      }
    } else {
      prepopFields.differentBillingAddress = true;
      prepopFields.differentName = true;
    }

    return prepopFields;
  }

}
