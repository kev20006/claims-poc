import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import { FlightComponent} from './components/flight-component/flight-component.component';
import { GenericCaseIntroComponent } from './components/generic-case-intro-component/generic-case-intro-component.component';
import { PaxComponent } from './components/pax-component/pax-component.component';
import { BankDetailsComponent } from './components/bank-details-component/bank-details-component.component';
import { ClaimComponent } from './pages/claim/claim.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    FlightComponent,
    GenericCaseIntroComponent,
    PaxComponent,
    BankDetailsComponent,
    ClaimComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
