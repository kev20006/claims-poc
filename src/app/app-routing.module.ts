import { NgModule } from '@angular/core';
import { Routes, RouterModule, provideRoutes } from '@angular/router';
import { BankDetailsComponent } from './components/bank-details-component/bank-details-component.component';
import { FlightComponent } from './components/flight-component/flight-component.component';
import { GenericCaseIntroComponent } from './components/generic-case-intro-component/generic-case-intro-component.component';
import { PaxComponent } from './components/pax-component/pax-component.component';
import { ClaimComponent } from './pages/claim/claim.component';
import { IndexComponent } from './pages/index/index.component';

const routes: Routes = [
  {path: 'claim', component: ClaimComponent, children: [
    {path: 'intro', component: GenericCaseIntroComponent},
    {path: 'pax', component: PaxComponent},
    {path: 'bankdetails', component: BankDetailsComponent},
    {path: 'flight', component: FlightComponent},
  ]},
  {path: '', component: IndexComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
