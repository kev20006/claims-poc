import { Component, OnInit } from '@angular/core';
import { ICaseComponents } from 'src/app/interfaces/CaseComponents';
import { CaseTypeService } from 'src/app/services/case-type.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  public caseTypes: ICaseComponents[];

  constructor(private caseTypeService: CaseTypeService) { }

  ngOnInit(): void {
    this.caseTypes = this.caseTypeService.getCaseTypes();
  }

}
