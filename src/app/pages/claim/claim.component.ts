import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs';

import { CaseTypeService } from 'src/app/services/case-type.service';
import { ClaimService } from 'src/app/services/claim.service';

@Component({
  selector: 'app-claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.scss']
})
export class ClaimComponent implements OnInit {

  public currentIndex: number;
  public formData: Observable<any>;
  public totalSteps: number;

  constructor(
    private claims: ClaimService,
    private caseTypeService: CaseTypeService,
    private activeRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this.activeRoute.queryParams.subscribe(params => {
      const caseComponents = this.caseTypeService.getCaseComponents(params.claimType);
      this.totalSteps = caseComponents.length;
      // tslint:disable-next-line: deprecation
      this.claims.currentComponentIndex.subscribe({
        next: (data) => this.currentIndex = data,
        error: (err) => console.log(err)
      });
      this.claims.initNewClaim(caseComponents);
      this.router.navigate(['claim', this.claims.getCurrentRoute()]);
      this.formData = this.claims.formDetails;

    });
  }

  public next(): void {
    this.router.navigate(['claim', this.claims.getNextRoute()]);
  }

  public back(): void {
    this.router.navigate(['claim', this.claims.getPreviousRoute()]);
  }

}
