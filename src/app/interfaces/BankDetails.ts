export interface BankDetails {
    hasBeenPrepopped: boolean;
    name: string;
    differentName: boolean;
    sortCode: string;
    accountNumber: string;
    differentBillingAddress: boolean;
    billingAddress: {
        address: string;
        postcode: string;
        city: string;
        country: string;
    };
}
