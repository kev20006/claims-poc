export interface PaxDetails {
    name: string;
    dob: Date;
    address: string;
    postcode: string;
    city: string;
    country: string;
}