import { PaxComponent } from '../components/pax-component/pax-component.component';
import { BankDetails } from './BankDetails';
import { FlightDetials } from './FlightDetails';
import { PaxDetails } from './PaxDetails';

export interface CaseDetails {
  genericCaseIntroComponent?: any;
  paxComponent?: PaxDetails;
  flightComponent?: FlightDetials;
  BankDetailsComponent?: BankDetails;
}
