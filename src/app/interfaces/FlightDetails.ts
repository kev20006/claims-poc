export interface FlightDetials {
    flightNumber: string;
    depDate: Date;
    depTime: Date;
}