import { Injectable } from '@angular/core';
import { ICaseComponents } from '../interfaces/CaseComponents';

@Injectable({
  providedIn: 'root'
})
export class CaseTypeService {

  private caseTypes: ICaseComponents[] = [
    { components: ['genericCaseIntroComponent', 'paxComponent', 'flightComponent', 'BankDetailsComponent']},
    { components: ['genericCaseIntroComponent', 'paxComponent', 'BankDetailsComponent']}
  ]

  constructor() { }

  public getCaseTypes(): ICaseComponents[] {
    return this.caseTypes;
  }

  public getCaseComponents(index: number): string[] {
    return this.caseTypes[index].components;
  }
}
