import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CaseDetails } from '../interfaces/CaseDetails';

const componentRouteMap = {
  genericCaseIntroComponent: 'intro',
  paxComponent: 'pax',
  flightComponent: 'flight',
  BankDetailsComponent: 'bankdetails'
};

const componentModelMap = {
  genericCaseIntroComponent: {},
  paxComponent: {
    name: '',
    dob: null,
    address: '',
    postcode: '',
    city: '',
    country: ''
  },
  flightComponent: {
    flightNumber: '',
    depDate: null,
    depTime: null
  },
  BankDetailsComponent: {
    hasBeenPrepopped: false,
    name: '',
    differentName: false,
    sortCode: '',
    accountNumber: '',
    differentBillingAddress: false,
    billingAddress: {
        address: '',
        postcode: '',
        city: '',
        country: ''
    }
  }
};

@Injectable({
  providedIn: 'root'
})
export class ClaimService {

  private caseComponents: string[];
  private claimRoutes: string[];
  // tslint:disable-next-line: variable-name
  private _currentComponentIndex: BehaviorSubject<number> = new BehaviorSubject(0);
  // tslint:disable-next-line: variable-name
  private _formDetails: BehaviorSubject<CaseDetails> = new BehaviorSubject({});

  public readonly currentComponentIndex: Observable<number> = this._currentComponentIndex.asObservable();
  public readonly formDetails: Observable<CaseDetails> = this._formDetails.asObservable();

  constructor() {}

  public initNewClaim(caseComponents: string[]): void{
    this.caseComponents = caseComponents;
    this.claimRoutes = this.caseComponents.map(component => componentRouteMap[component]);
    this._currentComponentIndex.next(0);

    const newFormDetails: any = {};
    for (const componentName of this.caseComponents) {
      newFormDetails[componentName] = componentModelMap[componentName];
    }
    this._formDetails.next(newFormDetails);
  }

  public getCurrentRoute(): string {
    return this.claimRoutes[this._currentComponentIndex.getValue()];
  }

  public getNextRoute(): string {
    this._currentComponentIndex.next(this._currentComponentIndex.getValue() + 1);
    return this.claimRoutes[this._currentComponentIndex.getValue()];
  }

  public getPreviousRoute(): string {
    this._currentComponentIndex.next(this._currentComponentIndex.getValue() - 1);
    return this.claimRoutes[this._currentComponentIndex.getValue()];
  }

  public addToFrom(component: string, payload: any): void {
    const formdetails = {...this._formDetails.getValue()}
    formdetails[component] = {...formdetails[component], ...payload};
    this._formDetails.next(formdetails);
  }
}
